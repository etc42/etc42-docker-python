etc42 package
=============

Subpackages
-----------

.. toctree::

    etc42.etc42
    etc42.tests

Module contents
---------------

.. automodule:: etc42
    :members:
    :undoc-members:
    :show-inheritance:
