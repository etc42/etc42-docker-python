import sys
import os
from setuptools import setup, find_packages
from etc42.etc42 import __version__

NAME = 'etc42' #PEP 508
VERSION = __version__   #PEP 440
# Add any necessary entry points
entry_points = {}
# Command-line scripts
entry_points['console_scripts'] = [
    'etc42 = etc42.etc42.etc42:main'
]
setup(name=NAME,
      version=VERSION,
      description='ETC42 Universal Exposure Time Calculator',
      long_description=open(os.path.join(os.path.dirname(__file__),
                            'README.md')).read(),
      url='https://lam.fr/',
      author='The LAM ETC42 developers',
      license='GPLv3',
      packages=find_packages(),
      requires= [],
      setup_requires=[],
      install_requires = [ 'docker>=2.0.0', 'wget>=1.0'],
      classifiers=[
          'Development Status :: 1 - Planning',
          'Intended Audience :: Science/Research',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)', # QUESTION : same as above
          'Topic :: Scientific/Engineering :: Astronomy',
          'Topic :: Scientific/Engineering :: Physics'
      ],
      include_package_data=True,
      entry_points=entry_points,
      tests_require=['pytest']
)
